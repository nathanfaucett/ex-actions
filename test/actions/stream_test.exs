defmodule Test.Actions.StreamTest do
  use ExUnit.Case
  doctest Actions

  alias Test.Actions.{CountAction, CountStream}

  test "Stream Fulfilled" do
    {:ok, action} = CountAction.create(%{count: 1})

    stream = CountStream.stream()

    CountStream.broadcast(action)

    fulfilled_action =
      stream
      |> Enum.find(fn action ->
        CountAction.Fulfilled.is_action(action)
      end)

    assert fulfilled_action.ref_id == action.id
    assert fulfilled_action.type == CountAction.Fulfilled.action_type()
    assert fulfilled_action.ref_type == CountAction.Fulfilled.ref_action_type()
    assert fulfilled_action.payload == %{count: 1}
  end

  test "Stream Rejected" do
    {:ok, action} = CountAction.create(%{count: -1})

    stream = CountStream.stream()

    CountStream.broadcast(action)

    rejected_action =
      stream
      |> Enum.find(fn action ->
        CountAction.Rejected.is_action(action)
      end)

    assert rejected_action.ref_id == action.id
    assert rejected_action.type == CountAction.Rejected.action_type()
    assert rejected_action.ref_type == CountAction.Rejected.ref_action_type()
    assert rejected_action.payload == %{message: "Count must be greater than 0"}
  end
end
