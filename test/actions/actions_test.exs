defmodule Test.Actions.ActionsTest do
  use ExUnit.Case
  doctest Actions

  alias Test.Actions.{CountAction}

  test "Action" do
    {:ok, action} = CountAction.create(%{count: 1})

    assert CountAction.is_action(action)

    assert action.type == CountAction.action_type()
    assert action.payload == %{count: 1}
  end

  test "Fulfilled" do
    {:ok, action} = CountAction.create(%{count: 1})
    {:ok, fulfilled_action} = CountAction.Fulfilled.create(action, %{count: 2})

    assert CountAction.Fulfilled.is_action(fulfilled_action)

    assert fulfilled_action.ref_id == action.id
    assert fulfilled_action.type == CountAction.Fulfilled.action_type()
    assert fulfilled_action.ref_type == CountAction.Fulfilled.ref_action_type()
    assert fulfilled_action.payload == %{count: 2}
  end

  test "Rejected" do
    {:ok, action} = CountAction.create(%{count: 1})
    {:ok, rejected_action} = CountAction.Rejected.create(action, %{message: "rejected"})

    assert CountAction.Rejected.is_action(rejected_action)

    assert rejected_action.ref_id == action.id
    assert rejected_action.type == CountAction.Rejected.action_type()
    assert rejected_action.ref_type == CountAction.Rejected.ref_action_type()
    assert rejected_action.payload == %{message: "rejected"}
  end
end
