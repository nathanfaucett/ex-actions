defmodule Test.Actions.HandlerTest do
  use ExUnit.Case
  doctest Actions

  alias Test.Actions.{CountStream, CountAction}

  test "Handler" do
    handler_task =
      Task.async(fn ->
        CountStream.stream()
        |> Enum.find(fn action ->
          CountAction.Rejected.is_action(action)
        end)
      end)

    action_task =
      Task.async(fn ->
        {:ok, action} = CountAction.create(%{count: 1})
        CountStream.broadcast(action)
        {:ok, action} = CountAction.create(%{count: -1})
        CountStream.broadcast(action)
      end)

    Task.await(action_task)
    action_rejected = Task.await(handler_task)

    assert CountAction.Rejected.is_action(action_rejected)
  end
end
