defmodule Test.Actions.AsyncTest do
  use ExUnit.Case
  doctest Actions

  alias Test.Actions.{CountAction, CountStream}

  test "Async" do
    {:ok, fulfilled_action} = CountStream.async(CountAction, %{count: 999})
    assert CountAction.Fulfilled.is_action(fulfilled_action)
  end
end
