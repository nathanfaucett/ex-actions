defmodule Test.Actions do
  defmodule CountAction do
    use Actions.AsyncAction

    def create_payload(%{} = payload) do
      {:ok, payload}
    end

    def create_fulfilled_payload(%{} = payload) do
      {:ok, payload}
    end

    def create_rejected_payload(%{} = payload) do
      {:ok, payload}
    end
  end

  defmodule CountHandler do
    use Actions.Handler

    require Logger

    alias Test.Actions.{CountAction, CountStream}

    def handle(%Actions.Action{} = action) do
      cond do
        CountAction.is_action(action) ->
          %{"count" => count} = action.payload

          if count > 0 do
            {:ok, fulfilled_action} = CountAction.Fulfilled.create(action, %{count: count})
            CountStream.broadcast(fulfilled_action)
          else
            {:ok, rejected_action} =
              CountAction.Rejected.create(action, %{message: "Count must be greater than 0"})

            CountStream.broadcast(rejected_action)
          end

        CountAction.Fulfilled.is_action(action) ->
          %{"count" => count} = action.payload
          Logger.debug("Count #{inspect(count)}")

        CountAction.Rejected.is_action(action) ->
          Logger.error(inspect(action.payload))
      end
    end
  end

  defmodule Handler do
    use Actions.Handler, [Test.Actions.CountHandler]
  end

  defmodule CountStream do
    use Actions.Stream

    alias Test.Actions.Handler

    def on_broadcast(%{} = action) do
      Handler.handle_action(Jason.decode!(Jason.encode!(action)))
    end
  end

  defmodule CountApplication do
    use Application

    alias Test.Actions.CountStream

    def start(_type, _args) do
      children = [
        CountStream
      ]

      Supervisor.start_link(children, strategy: :one_for_one)
    end
  end

  CountApplication.start(:normal, [])
end

ExUnit.start()
