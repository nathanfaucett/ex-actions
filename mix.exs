defmodule Actions.MixProject do
  use Mix.Project

  def project do
    [
      app: :actions,
      version: "0.1.0",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps() ++ deps(Mix.env())
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps(),
    do: [
      {:phoenix_ecto, "~> 4.0"},
      {:jason, "~> 1.1"}
    ]

  defp deps(:prod), do: []
  defp deps(:dev), do: []
  defp deps(:test), do: []
end
