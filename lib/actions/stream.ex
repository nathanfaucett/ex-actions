defmodule Actions.Stream do
  defmacro __using__(_opts) do
    quote do
      use Supervisor

      require Logger

      @default_control_timeout 30_000

      @callback on_broadcast(action :: map()) :: any()

      defmodule Streams do
        def start_link() do
          Agent.start_link(fn -> %{} end, name: __MODULE__)
        end

        def broadcast(action) do
          Agent.get(__MODULE__, fn streams ->
            Enum.each(streams, fn {pid, _} ->
              send(pid, {:action, action})
            end)
          end)
        end

        def put(pid) do
          Agent.update(__MODULE__, fn streams ->
            Map.put(streams, pid, nil)
          end)

          pid
        end

        def delete(pid) do
          Agent.update(__MODULE__, fn streams ->
            Map.delete(streams, pid)
          end)

          pid
        end
      end

      def start_link(opts \\ []) do
        Supervisor.start_link(__MODULE__, [], name: __MODULE__)
      end

      def init(_opts \\ []) do
        children = [
          worker(Streams, [], restart: :temporary)
        ]

        supervise(children, strategy: :one_for_one)
      end

      def broadcast(%{} = action) do
        on_broadcast(action)
        Streams.broadcast(action)
      end

      def async(module, args \\ %{}) do
        case apply(module, :create, [args]) do
          {:ok, action} ->
            actions_stream = stream()

            broadcast(action)

            action =
              actions_stream
              |> Enum.find(fn action ->
                apply(Module.concat([module, Fulfilled]), :is_action, [action]) ||
                  apply(Module.concat([module, Rejected]), :is_action, [action])
              end)

            if apply(Module.concat([module, Fulfilled]), :is_action, [action]) do
              {:ok, action}
            else
              {:error, action}
            end

          result ->
            result
        end
      end

      def stream(options \\ []) do
        timeout = Keyword.get(options, :timeout, @default_control_timeout)
        create_stream(self(), timeout)
      end

      def stop(pid, options \\ []) do
        timeout = Keyword.get(options, :timeout, @default_control_timeout)

        send(pid, {:stop, self()})

        receive do
          :ok -> :ok
        after
          timeout -> :timeout
        end
      end

      defp create_stream(processor, timeout) do
        pid = spawn_async(processor)

        Stream.resource(
          fn -> {processor, pid} end,
          fn {processor, pid} -> receive_next(pid, processor, timeout) end,
          fn {_processor, pid} ->
            if pid != nil do
              send(pid, {:cancel, self()})
            end
          end
        )
      end

      defp async_handler(processor) do
        receive do
          {:action, action} ->
            send(processor, {:action, action})
            async_handler(processor)
        end
      end

      defp spawn_async(processor) do
        pid =
          spawn(fn ->
            async_handler(processor)
          end)

        Streams.put(pid)
      end

      defp receive_next(pid, processor, timeout) do
        receive do
          {:action, action} ->
            {[action], {processor, pid}}

          {:stop, requester} ->
            send(pid, {:cancel, self()})
            send(requester, :ok)
            Streams.delete(pid)
            {:halt, {processor, pid}}

          _ ->
            receive_next(pid, processor, timeout)
        after
          timeout ->
            Logger.debug("Timeout, stopping stream.")
            send(pid, {:cancel, self()})
            Streams.delete(pid)
            {:halt, {processor, pid}}
        end
      end
    end
  end
end
