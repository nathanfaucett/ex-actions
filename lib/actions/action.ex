defmodule Actions.Action do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @derive Jason.Encoder
  schema "" do
    field(:ref_id, :binary_id)
    field(:ref_type, :string)
    field(:type, :string)
    field(:payload, :map, default: %{})
    field(:created_at, :utc_datetime)
  end

  def create(type, %{} = payload) do
    changeset =
      %Actions.Action{}
      |> cast(%{type: type, payload: payload}, [:type, :payload])
      |> put_change(:id, Ecto.UUID.generate())
      |> put_change(:created_at, DateTime.utc_now())
      |> validate_required([:id, :type, :payload, :created_at])

    if changeset.valid?() do
      {:ok, changeset.changes}
    else
      {:error, changeset}
    end
  end

  def create!(type, %{} = payload) do
    case create(type, payload) do
      {:ok, action} ->
        action

      {:error, changeset} ->
        throw(changeset)
    end
  end

  def create(type, %{} = ref_action, %{} = payload) do
    changeset =
      %Actions.Action{}
      |> cast(
        %{
          ref_id: Map.get(ref_action, :id, Map.get(ref_action, "id")),
          ref_type: Map.get(ref_action, :type, Map.get(ref_action, "type")),
          type: type,
          payload: payload
        },
        [:ref_id, :ref_type, :type, :payload]
      )
      |> put_change(:id, Ecto.UUID.generate())
      |> put_change(:created_at, DateTime.utc_now())
      |> validate_required([:id, :ref_id, :ref_type, :type, :payload, :created_at])

    if changeset.valid?() do
      {:ok, changeset.changes}
    else
      {:error, changeset}
    end
  end

  def create!(type, %{} = ref_action, %{} = payload) do
    case create(type, ref_action, payload) do
      {:ok, action} ->
        action

      {:error, changeset} ->
        throw(changeset)
    end
  end

  defmacro __using__([ref_action_type, action_type]) do
    quote do
      @callback create_payload(payload :: term) ::
                  {:ok, payload :: term} | {:error, reason :: term}

      def ref_action_type(), do: unquote(ref_action_type)
      def action_type(), do: unquote(action_type)

      def is_action(%{} = action),
        do:
          (Map.has_key?(action, :ref_id) || Map.has_key?(action, "ref_id")) and
            (action_type() == Map.get(action, :type) || action_type() == Map.get(action, "type")) and
            (ref_action_type() == Map.get(action, :ref_type) ||
               ref_action_type() == Map.get(action, "ref_type"))

      def is_action(_action),
        do: false

      def is_not_action(action), do: not is_action(action)

      def create(%{} = ref_action, %{} = payload) do
        case create_payload(payload) do
          {:ok, payload} ->
            Actions.Action.create(action_type(), ref_action, payload)

          result ->
            result
        end
      end

      def create!(%{} = ref_action, %{} = payload) do
        case create_payload(payload) do
          {:ok, payload} ->
            Actions.Action.create(action_type(), ref_action, payload)

          {:error, error} ->
            throw(error)
        end
      end
    end
  end

  defmacro __using__(action_type) when is_bitstring(action_type) do
    quote do
      @callback create_payload(payload :: term) ::
                  {:ok, payload :: term} | {:error, reason :: term}

      def action_type(), do: unquote(action_type)

      def is_action(%{} = action),
        do: action_type() == Map.get(action, :type) || action_type() == Map.get(action, "type")

      def is_action(_action),
        do: false

      def is_not_action(action), do: not is_action(action)

      def create(%{} = payload) do
        case create_payload(payload) do
          {:ok, payload} ->
            Actions.Action.create(action_type(), payload)

          result ->
            result
        end
      end

      def create!(%{} = payload) do
        case create_payload(payload) do
          {:ok, payload} ->
            Actions.Action.create(action_type(), payload)

          {:error, error} ->
            throw(error)
        end
      end
    end
  end

  defmacro __using__(opts) when length(opts) == 0 do
    quote do
      @callback create_payload(payload :: term) ::
                  {:ok, payload :: term} | {:error, reason :: term}

      def action_type(), do: String.downcase(to_string(__MODULE__))

      def is_action(%{} = action),
        do: action_type() == Map.get(action, :type) || action_type() == Map.get(action, "type")

      def is_action(_action),
        do: false

      def is_not_action(action), do: not is_action(action)

      def create(%{} = payload) do
        case create_payload(payload) do
          {:ok, payload} ->
            Actions.Action.create(action_type(), payload)

          result ->
            result
        end
      end

      def create!(%{} = payload) do
        case create_payload(payload) do
          {:ok, payload} ->
            Actions.Action.create(action_type(), payload)

          {:error, error} ->
            throw(error)
        end
      end
    end
  end

  defmacro __using__(ref_action) do
    quote do
      @callback create_payload(payload :: term) ::
                  {:ok, payload :: term} | {:error, reason :: term}

      def ref_action_type(), do: String.downcase(to_string(unquote(ref_action)))
      def action_type(), do: String.downcase(to_string(__MODULE__))

      def is_action(%{} = action),
        do:
          (Map.has_key?(action, :ref_id) || Map.has_key?(action, "ref_id")) and
            (action_type() == Map.get(action, :type) || action_type() == Map.get(action, "type")) and
            (ref_action_type() == Map.get(action, :ref_type) ||
               ref_action_type() == Map.get(action, "ref_type"))

      def is_action(_action),
        do: false

      def is_not_action(action), do: not is_action(action)

      def create(%{} = ref_action, %{} = payload) do
        case create_payload(payload) do
          {:ok, payload} ->
            Actions.Action.create(action_type(), ref_action, payload)

          result ->
            result
        end
      end

      def create!(%{} = ref_action, %{} = payload) do
        case create_payload(payload) do
          {:ok, payload} ->
            Actions.Action.create(action_type(), ref_action, payload)

          {:error, error} ->
            throw(error)
        end
      end
    end
  end
end
