defmodule Actions.Handler do
  def to_struct(kind, attrs) do
    struct = struct(kind)

    Enum.reduce(Map.to_list(struct), struct, fn {k, _}, acc ->
      case Map.get(attrs, k, Map.get(attrs, Atom.to_string(k))) do
        nil -> acc
        v -> %{acc | k => v}
      end
    end)
  end

  defmacro __using__(handlers) when is_list(handlers) and length(handlers) != 0 do
    quote do
      def handle_action(%{} = action) do
        action = Actions.Handler.to_struct(Actions.Action, action)

        unquote(handlers)
        |> Enum.reduce(nil, fn handler, acc ->
          apply(handler, :handle_action, [action])
        end)
      end
    end
  end

  defmacro __using__(_opts) do
    quote do
      @callback handle(action :: Actions.Action.t()) :: any()

      def handle_action(%{} = action) do
        handle(Actions.Handler.to_struct(Actions.Action, action))
      end
    end
  end
end
