defmodule Actions.AsyncAction do
  defmacro __using__(action_type) when is_bitstring(action_type) do
    quote do
      use Actions.Action, unquote(action_type)

      @callback create_fulfilled_payload(payload :: term) ::
                  {:ok, payload :: term} | {:error, reason :: term}
      @callback create_rejected_payload(payload :: term) ::
                  {:ok, payload :: term} | {:error, reason :: term}

      defmodule Fulfilled do
        use Actions.Action, [unquote(action_type), "#{unquote(action_type)}.fulfilled"]

        def create_payload(%{} = payload) do
          apply(
            __MODULE__ |> Module.split() |> Enum.drop(-1) |> Module.concat(),
            :create_fulfilled_payload,
            [payload]
          )
        end
      end

      defmodule Rejected do
        use Actions.Action, [unquote(action_type), "#{unquote(action_type)}.rejected"]

        def create_payload(%{} = payload) do
          apply(
            __MODULE__ |> Module.split() |> Enum.drop(-1) |> Module.concat(),
            :create_rejected_payload,
            [payload]
          )
        end
      end
    end
  end

  defmacro __using__(_opts) do
    quote do
      use Actions.Action

      @callback create_fulfilled_payload(payload :: term) ::
                  {:ok, payload :: term} | {:error, reason :: term}
      @callback create_rejected_payload(payload :: term) ::
                  {:ok, payload :: term} | {:error, reason :: term}

      defmodule Fulfilled do
        use Actions.Action, __MODULE__ |> Module.split() |> Enum.drop(-1) |> Module.concat()

        def create_payload(%{} = payload) do
          apply(
            __MODULE__ |> Module.split() |> Enum.drop(-1) |> Module.concat(),
            :create_fulfilled_payload,
            [payload]
          )
        end
      end

      defmodule Rejected do
        use Actions.Action, __MODULE__ |> Module.split() |> Enum.drop(-1) |> Module.concat()

        def create_payload(%{} = payload) do
          apply(
            __MODULE__ |> Module.split() |> Enum.drop(-1) |> Module.concat(),
            :create_rejected_payload,
            [payload]
          )
        end
      end
    end
  end
end
